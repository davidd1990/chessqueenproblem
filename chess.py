
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


matriz = [
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
]


def solveQueens (matriz, n, row):
    if n == row:
         return True
    else:

        for y in range(n):
            if isValid(matriz, row, y):
                matriz[row][y] = 1
                if solveQueens(matriz, n, row + 1):
                    return True
            matriz[row][y] = 0

        return False


def marcarcasillas (matriz, x, y):
    matriz[x][y] = 'Q'


def isValid(matriz, x,y):
    for i in range(8):
        if matriz[x][i]:
            return False
    for i in range(8):
        if matriz[i][y]:
            return False
    if marcardiagonales(matriz, x, y):
        return False

    return True






def marcardiagonales (matriz, x,y):
    if diagonal_inferior(matriz,x,y):
        return True
    if diagonal_superior(matriz, x, y):
        return True
    return False




def diagonal_superior(matriz, x, y ):
    for z in range(len(matriz) - (len(matriz) - (x + 1))):
        w = x - z
        p = y - z
        if p >= 0 and w >= 0:
            if matriz[w][p]:
                return True

    for h in range((len(matriz) - (y + 1)) + 1):
        w = x - h
        p = y + h
        if p >= 0 and w >= 0:
            if matriz[w][p]:
                return True

    return False


def diagonal_inferior(matriz, x, y ):
    for z in range( (len(matriz) -( x + 1)) + 1):
        w = x + z
        p = y - z
        if p <= len(matriz) and w <= len(matriz) and p >= 0 and w >= 0 :
            if matriz[w][p]:
                return True

    for h in range((len(matriz) - (y + 1)) + 1):
        w = x + h
        p = y + h
        if p <= (len(matriz) - 1) and w <= (len(matriz) -1):
            if matriz[w][p]:
                return True

    return False

def imprimir (matriz):
    if (solveQueens(matriz,8,0)):
        print('\n'.join([''.join(['{:4}'.format(item) for item in row])
                         for row in matriz]))
    else:
        print('respuesta no encontrada')

imprimir(matriz)